/* 
 * File:   server.hpp
 * Author: Roman Tomjak
 *
 * Created on May 15, 2012, 6:30 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERVER_HPP
#define	SERVER_HPP

#define RECV_BUFF 256
#define SEND_BUFF 256

#include <winsock2.h>
#include <sstream>
#include <string>
#include <vector>

#include "globals.hpp"


struct Thread_Info; // forward declaration


// contains parsed http request
struct HTTP_Request {
    std::string method;
    std::string uri;
};


// contains http headers
struct HTTP_Response {
    std::string http_code;
    std::string content_type;
    std::string file_name;
    std::string date;
    std::string server;

    HTTP_Response();
};


// contains shared file info
struct Share {
    std::string screen_name;
    std::string file_name;
    std::string uri;

    struct {
        __int64 bytes;
        char readable[10];
    } file_size;

};


/*
 * contains each individual client's http request, response
 * addres, file id, etc.
 */
struct HTTP_Client {
    SOCKET sock;
    sockaddr addr;
    std::stringstream request;
    std::stringstream response;
    std::stringstream responseBody;
    bool hasFullRequest;
    bool headersSent;
    bool fileTransferInProgress;

    struct {
        __int64 bytesSent;
        int id;
    } file;
};


// THE HTTP server :)
class HTTPD {
private:
    int nClient;                        // currently connected clients
    SOCKET server_socket;               // listening socket
    sockaddr_in local_addr;             // server address
    HTTP_Client clients[MAX_CLIENTS];   // server client struct
    std::vector<Share*> files;          // shared files

    void parse_http(HTTP_Request &request, std::string request_str);

public:
    HTTPD(char *host, int port);

    void accept_conn(SOCKET conn);
    void close(SOCKET client);
    void exit();
    
    void read_request(SOCKET sock);
    int respond_to_request(SOCKET sock);

    void share_directory(char *path);
    void share_file(char *filename);

    void send_index(int CID, SOCKET sock, bool only_headers);
    void send_404(int CID, SOCKET sock, std::string uri);

    int match_file(std::string uri);
    static void start_thread(void *param);
    int send_file(SOCKET sock);
    int set_file_headers(int CID, SOCKET sock);
    void *read_and_send(Thread_Info *ti);

    char *server_ip();
    u_short server_port();
    char *server_ip_port();
    std::string server_date();
};


// struct for passing data to the file i/o thread
struct Thread_Info {
    HTTPD *server;
    SOCKET sock;
};

#endif	/* SERVER_HPP */

