/* 
 * File:   main.cpp
 * Author: Roman Tomjak
 * 
 * Created on May 14, 2012, 11:20 PM
 * 
 * KWIVA - A "web-share" client
 * Copyright (C) 2012  Roman Tomjak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _WIN32_IE 0x0400

#include <windows.h>
#include <winsock2.h>
#include <commctrl.h>

#include "resource.hpp" // for GUI controls
#include "globals.hpp"  // global functions, structs, f(x)...
#include "main.hpp"     // f(x) prototypes
#include "server.hpp"   // server class (send/recv/parse/etc)


int WINAPI WinMain(HINSTANCE hThisInstance,
        HINSTANCE hPrevInstance,
        LPSTR lpszArgument,
        int nCmdShow) {

    WSADATA wsaData; // winsock dll info
    MSG messages; // here messages to the application are saved
    WNDCLASSEX wincl; // data structure for the windowclass
    INITCOMMONCONTROLSEX InitCtrlEx; // structure for form control init


    // the window structure
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure; // this function is called by windows
    wincl.style = CS_DBLCLKS; // catch double-clicks
    wincl.cbSize = sizeof (WNDCLASSEX);
    wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL; // no menu
    wincl.cbClsExtra = 0; // no extra bytes after the window class
    wincl.cbWndExtra = 0; // structure or the window instance
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;


    // set app icon
    wincl.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_APP_ICON));
    wincl.hIconSm = (HICON) LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_APP_ICON), IMAGE_ICON, 16, 16, 0);


    // init common controls
    InitCtrlEx.dwSize = sizeof (INITCOMMONCONTROLSEX);
    InitCtrlEx.dwICC = ICC_PROGRESS_CLASS;
    InitCommonControlsEx(&InitCtrlEx);


    // init winsock
    // winsock 2.2 = (98, ME, NT4, 2000 and XP)
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) < 0)
        die("Unable to load Winsock 2 DLL.");


    // Register the window class, and if it fails quit the program
    if (!RegisterClassEx(&wincl))
        return 0;


    // The class is registered, let's create the program
    g_hWnd = CreateWindowEx(
            WS_EX_ACCEPTFILES, // accept drag n drop
            szClassName, // Classname
            "KWIVA - WEB-Share Client", // Title Text
            WS_OVERLAPPEDWINDOW, // default window
            CW_USEDEFAULT, // Windows decides the position
            CW_USEDEFAULT, // where the window ends up on the screen
            APP_WIDTH, // The programs width
            APP_HEIGHT, // and height in pixels
            HWND_DESKTOP, // The window is a child-window to desktop
            NULL, // No menu
            hThisInstance, // Program Instance handler
            NULL // No Window Creation data
            );

    
    // center window
    int xpos = (GetSystemMetrics(SM_CXSCREEN) - APP_WIDTH) / 2;
    int ypos = (GetSystemMetrics(SM_CYSCREEN) - APP_HEIGHT) / 2;
    MoveWindow(g_hWnd, xpos, ypos, APP_WIDTH, APP_HEIGHT, 1);

    
    // make the window visible on the screen
    ShowWindow(g_hWnd, nCmdShow);


    // create server and init client sockets
    g_Server = new HTTPD("127.0.0.1", 80);


    // create all gui elements
    initGUI();


    // Run the message loop. It will run until GetMessage() returns 0
    while (GetMessage(&messages, NULL, 0, 0)) {
        // Translate virtual-key messages into character messages
        TranslateMessage(&messages);

        // Send message to WindowProcedure
        DispatchMessage(&messages);
    }

    // The program return-value is 0 - The value that PostQuitMessage() gave
    return messages.wParam;
}


//  This function is called by the Windows function DispatchMessage()
LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

    // handle the messages
    switch (message) {

        case WM_DESTROY:
            // gracefully stop
            g_Server->exit();
            WSACleanup();

            PostQuitMessage(0); // send a WM_QUIT to the message queue
            break;


        case WM_SOCKET:
            switch (WSAGETSELECTEVENT(lParam)) {

                case FD_READ:
                    {
                        g_Server->read_request(wParam);
                        g_Server->respond_to_request(wParam);
                    }
                    break;


                case FD_CLOSE:
                    {
                        g_Server->close(wParam);
                    }
                    break;


                case FD_ACCEPT:
                    {
                        g_Server->accept_conn(wParam);
                    }
                    break;

                case FD_WRITE:
                    {
                        g_Server->send_file(wParam);
                    }
                    break;

            }
            break;

        case WM_DROPFILES:
            {
                HDROP hDropInfo = (HDROP) wParam;
                char sItem[MAX_PATH];

                for (int i = 0; DragQueryFile(hDropInfo, i, (LPSTR) sItem, sizeof (sItem)); i++) {
                    if (GetFileAttributes(sItem) & FILE_ATTRIBUTE_DIRECTORY) {
                        g_Server->share_directory(sItem);
                    } else {
                        g_Server->share_file(sItem);
                    }
                }
                DragFinish(hDropInfo);
            }
            break;
                 
        case WM_SIZE:
            {
                // respond to window size changes
                HWND hEdit1, hEdit2;
                RECT rcClient;

                GetClientRect(g_hWnd, &rcClient);

                hEdit1 = GetDlgItem(g_hWnd, IDC_LISTVIEW);
                hEdit2 = GetDlgItem(g_hWnd, IDC_STATUSBAR);

                // xpos, ypos, xsize, ysize
                if (DEBUG) {
                    HWND hEdit3 = GetDlgItem(g_hWnd, IDC_EDIT_IN);
                    
                    SetWindowPos(hEdit1, NULL, 0, 0, rcClient.right, rcClient.bottom-200, SWP_NOZORDER);
                    SetWindowPos(hEdit2, NULL, 0, rcClient.bottom-20, rcClient.right, 20, SWP_NOZORDER);
                    SetWindowPos(hEdit3, NULL, 0, rcClient.bottom-195, rcClient.right, 173, SWP_NOZORDER);
                } else {
                    SetWindowPos(hEdit1, NULL, 0, 0, rcClient.right, rcClient.bottom-20, SWP_NOZORDER);
                    SetWindowPos(hEdit2, NULL, 0, rcClient.bottom-20, rcClient.right, 20, SWP_NOZORDER);
                }
            }
            break;

        default:
            // for messages that we don't deal with
            return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}
