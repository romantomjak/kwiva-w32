/* 
 * File:   main.hpp
 * Author: Roman Tomjak
 *
 * Created on May 15, 2012, 6:27 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAIN_HPP
#define	MAIN_HPP

#include "globals.hpp"


// declare Windows procedure
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

// make the class name into a global variable
char szClassName[] = "Kwiva";

void initGUI() {

    /*
     * Incoming message box
     * Only used for displaying HTTP requests
     */
    if (DEBUG) {
        g_hInputBox = CreateWindowEx(WS_EX_CLIENTEDGE,
                "EDIT",
                "",
                WS_CHILD | WS_VISIBLE | ES_MULTILINE |
                ES_AUTOVSCROLL | ES_AUTOHSCROLL,
                0,
                240,
                APP_WIDTH,
                180,
                g_hWnd,
                (HMENU) IDC_EDIT_IN,
                GetModuleHandle(NULL),
                NULL);

        // set font
        HGDIOBJ hfDefault = GetStockObject(DEFAULT_GUI_FONT);

        SendMessage(g_hInputBox,
                WM_SETFONT,
                (WPARAM) hfDefault,
                MAKELPARAM(FALSE, 0));
    }

    /*
     * File list box
     * Holds all share info (id, size, path, etc)
     */
    LV_COLUMN lvCol;

    g_hListView = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            WC_LISTVIEW,
            "",
            WS_VISIBLE | WS_CHILD | LVS_REPORT | LVS_SHOWSELALWAYS,
            0, 0, APP_WIDTH, 235,
            g_hWnd,
            (HMENU) IDC_LISTVIEW,
            GetModuleHandle(NULL),
            NULL
            );

    if (g_hListView == NULL)
        die("Unable to create list box.");
    
    // properly resize if debug text box is not shown
    if (!DEBUG)
        SetWindowPos(g_hListView, NULL, 0, 0, APP_WIDTH, APP_HEIGHT-60, SWP_NOZORDER);
        

    ListView_SetExtendedListViewStyle(g_hListView, LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    memset(&lvCol, 0, sizeof (lvCol));
    lvCol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
    
    lvCol.fmt = LVCFMT_CENTER;
    lvCol.pszText = "Size";
    lvCol.cx = 60;
    SendMessage(g_hListView, LVM_INSERTCOLUMN, 0, (LPARAM) & lvCol);
    
    lvCol.fmt = LVCFMT_LEFT;
    lvCol.pszText = "Path";
    lvCol.cx = 490;
    SendMessage(g_hListView, LVM_INSERTCOLUMN, 0, (LPARAM) & lvCol);

    lvCol.fmt = LVCFMT_CENTER;
    lvCol.pszText = "#";
    lvCol.cx = 30;
    SendMessage(g_hListView, LVM_INSERTCOLUMN, 0, (LPARAM) & lvCol);


    HFONT hFont = CreateFontA(
            13, 5, 0, 0, 400,
            FALSE, FALSE, FALSE, ANSI_CHARSET,
            OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
            DEFAULT_PITCH | FF_DONTCARE,
            "Tahoma"
            );

    SendMessage(g_hListView, WM_SETFONT, (WPARAM) hFont, MAKELPARAM(FALSE, 0));



    /*
     * Status bar
     * Displays current IP:PORT and some other cool info :)
     */
    g_hStatusBar = CreateWindowEx(
            0,
            STATUSCLASSNAME,
            NULL,
            SBARS_SIZEGRIP | WS_CHILD | WS_VISIBLE,
            0, 190, 600, 20,
            g_hWnd,
            (HMENU) IDC_STATUSBAR,
            GetModuleHandle(NULL),
            NULL);

    SendMessage(g_hStatusBar, SB_SETTEXT, (WPARAM) 0 | 0, (LPARAM) (LPSTR) g_Server->server_ip_port());
}

#endif	/* MAIN_HPP */

