#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc.exe
CCC=g++.exe
CXX=g++.exe
FC=gfortran
AS=as.exe

# Macros
CND_PLATFORM=MinGW_MSYS-Windows
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/globals.o \
	${OBJECTDIR}/server.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=-m32

# CC Compiler Flags
CCFLAGS=-m32 -mwindows
CXXFLAGS=-m32 -mwindows

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lcomctl32 -lshlwapi -lws2_32 -lgdi32 ${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}/resource.o

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/kwiva-w32.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/kwiva-w32.exe: ${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}/resource.o

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/kwiva-w32.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -static -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/kwiva-w32 ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/globals.o: globals.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/globals.o globals.cpp

${OBJECTDIR}/server.o: server.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/server.o server.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}/resource.o: resource.rc resource.hpp
	${MKDIR} -p ${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}
	@echo Compiling windows resource
	windres resource.rc ${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}/resource.o

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/kwiva-w32.exe
	${RM} ${CND_BUILDDIR}/${CONF}/${CND_PLATFORM_${CONF}}/resource.o

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
