========
Overview
========
KWIVA W32 is a simple file sharing application built using Winsock2 framework. It is designed to provide both a simple interface for file sharing, and an easy way to share files across the network. Only thing you will need besides this app is a web browser. Launch the app, drag-n-drop stuff you want to share and BOOM! Files are available for download from your network address.

Features
--------
Besides the fact that KWIVA is an extremely easy to use application for file sharing, it provides the following features:
 * Drag-n-drop support
 * Built in HTTP server
 * Concurrent downloads

Dependencies
------------
None. KWIVA is built to be a standalone executable for Windows OS.

Installation
------------
Not needed, just run the kwiva-w32.exe and start sharing your files.

License
-------

::

  KWIVA - A "web-share" client
  Copyright (C) 2012  Roman Tomjak

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
