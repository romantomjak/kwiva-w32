/* 
 * File:   globals.cpp
 * Author: Roman Tomjak
 * 
 * Created on May 15, 2012, 6:33 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cstring>
#include <cstdio>

#include "globals.hpp"


HWND g_hWnd, g_hInputBox, g_hListView, g_hStatusBar;
HTTPD *g_Server;

/*
 * Output error and quit
 * 
 * Usually used as a mean to notify user
 * and exit application.
 */
void die(char *message) {

  char *title = new char[30];
  char *buffer = new char[strlen(message)];

  sprintf(title, "Error #%d", WSAGetLastError());
  sprintf(buffer, "%s", message);

  MessageBox(g_hWnd, buffer, title, MB_OK | MB_ICONERROR);

  g_Server->exit();

  exit(1);
}

