/* 
 * File:   resource.hpp
 * Author: Roman Tomjak
 *
 * Created on May 15, 2012, 6:29 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RESOURCE_HPP
#define	RESOURCE_HPP

// EXE INFO
#define VOS_NT_WINDOWS32  0x00040004L
#define VFT_APP           0x00000001L

#define IDI_APP_ICON  1

#define IDC_EDIT_IN     100
#define IDC_LISTVIEW    101
#define IDC_STATUSBAR   102

#endif	/* RESOURCE_HPP */

