/* 
 * File:   server.cpp
 * Author: Roman Tomjak
 *
 * Created on May 14, 2012, 6:35 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <winsock2.h>
#include <shlwapi.h>
#include <commctrl.h>
#include <process.h>

#include <iostream>
#include <fstream>
#include <time.h>

#include "globals.hpp"
#include "server.hpp"

/*
 * HTTP_Response constructor
 * Initialises default values
 */
HTTP_Response::HTTP_Response() {
    this->http_code = "200 OK";
    this->content_type = "text/html";
    this->server = "KWIVA/";
    this->server.append(VERSION);
    this->file_name.assign("");
}


/*
 * Server constructor
 *
 * Open & init server socket on specified IP and port
 * http://www.w3.org/Protocols/rfc2616/rfc2616.html
 */
HTTPD::HTTPD(char *host, int port) {

    srand(time(NULL));  // init random seed
    this->nClient = 0;  // current client count


    // create a socket to send/receive data
    if ((this->server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        die("Unable to open socket");


    // set server ip and port
    memset(&this->local_addr, 0, sizeof (struct sockaddr_in));
    this->local_addr.sin_family = AF_INET;
    this->local_addr.sin_addr.s_addr = htonl(INADDR_ANY); // inet_addr(host)
    this->local_addr.sin_port = htons(port);


    // more than one process may bind to the same IP:PORT socket
    int flag = 1;
    if (setsockopt(this->server_socket, SOL_SOCKET, SO_REUSEADDR, (char *) &flag, sizeof (flag)) < 0)
        die("Unable to set reuse socket");


    // bind the socket to specified address and port.
    if (bind(this->server_socket, (SOCKADDR *) & this->local_addr, sizeof (this->local_addr)) < 0)
        die("Bind failed");


    // tell OS that we want to be notified when there's data to read, connection to accept, etc
    if (WSAAsyncSelect(this->server_socket, g_hWnd, WM_SOCKET, (FD_CLOSE | FD_ACCEPT | FD_READ | FD_WRITE)))
        die("Unable to set async sockets");


    // start to listen
    if (listen(this->server_socket, MAX_CLIENTS) < 0)
        die("Unable to listen!");
}


/*
 * Static f(x) for starting file transfer thread
 */
void HTTPD::start_thread(void* param) {
    Thread_Info *ti = new Thread_Info;
    ti = (Thread_Info *)param; // cast to correct data type
    
    reinterpret_cast<HTTPD*>(ti->server)->read_and_send(ti);
}


/*
 * Returns server's IP address
 */
char *HTTPD::server_ip() {
    return inet_ntoa(local_addr.sin_addr);
}


/*
 * Returns server's port
 */
u_short HTTPD::server_port() {
    return ntohs(local_addr.sin_port);
}


/*
 * Returns server's IP:PORT
 */
char *HTTPD::server_ip_port() {
    std::stringstream ss;

    ss << this->server_ip();
    ss << ":";
    ss << this->server_port();

    return (char *) ss.str().c_str();
}


/*
 * Reads http request
 * 
 * Sets hasFullRequest to true when sees "\r\n\r\n", e.g.,
 * end of HTTP request, so that other functions may begin
 * their work - parsing/responding/etc.
 */
void HTTPD::read_request(SOCKET remote_sock) {
    char *buffer = new char[RECV_BUFF];
    int bytesReceived;

    // find which socket has the data
    for (int i = 0; i < MAX_CLIENTS; i++) {
        if (this->clients[i].sock == remote_sock && this->clients[i].sock != INVALID_SOCKET) {
            // clean buffer
            memset(buffer, 0, RECV_BUFF);

            // read incoming data (null terminated)
            bytesReceived = recv(remote_sock, buffer, RECV_BUFF - 1, 0);

            // append to request stream
            this->clients[i].request << buffer;

            if (DEBUG)
                std::cout << "Bytes received: " << bytesReceived << std::endl;

            // check if we have the full request
            if (this->clients[i].request.str().find("\r\n\r\n") != std::string::npos) {
                this->clients[i].hasFullRequest = true;

                // shows http request
                if (DEBUG)
                    SendMessage(g_hInputBox, WM_SETTEXT, this->clients[i].request.str().size() + 1, reinterpret_cast<LPARAM> (this->clients[i].request.str().c_str()));
            }

            // socket found, data read - exit loop
            break;
        }
    }

    // clean up
    delete[] buffer;
}

/*
 * Parse HTTP request
 * 
 * Currently does very little - sets request method and uri in the
 * HTTP_Request object that is passed by reference
 */
void HTTPD::parse_http(HTTP_Request &http_request, std::string http_request_str) {
    size_t pos, pos2;

    if (http_request_str.compare(0, 3, "GET") == 0) {
        http_request.method = "GET";

        pos = http_request_str.find(" HTTP/1");
        http_request.uri = http_request_str.substr(4, pos - 4);

    } else if (http_request_str.compare(0, 4, "HEAD") == 0) {

        http_request.method = "HEAD";

        pos = http_request_str.find(" HTTP/1");
        http_request.uri = http_request_str.substr(5, pos - 5);
        
    } else {
        http_request.method = "UNSUPORTED";
    }

}

/*
 * Respond to http request
 * 
 * This is the main HTTPD function that uses parsed HTTP request
 * in order to decide which response should be sent. After the
 * response is sent the corresponding socket is closed.
 * 
 * You may think of this function as THE HTTP server, because all
 * of the server logic resides here :)
 */
int HTTPD::respond_to_request(SOCKET remote_sock) {
    int CID = 0;
    HTTP_Request http_request;
    HTTP_Response http_response;


    // find to which socket to respond
    for (CID = 0; CID < MAX_CLIENTS; CID++) {
        if (this->clients[CID].sock == remote_sock)
            if (this->clients[CID].hasFullRequest == true)
                break;
            else
                return -1;

        // socket not found, exit
        if (CID == MAX_CLIENTS - 1)
            return -1;
    }


    // clean buffers
    this->clients[CID].response.str("");
    this->clients[CID].responseBody.str("");
    this->parse_http(http_request, this->clients[CID].request.str());


    // send response
    if (http_request.method == "GET") {

        if (http_request.uri == "/") {

            // index requested
            this->send_index(CID, remote_sock, false);

            return 0;

        } else if ((this->clients[CID].file.id = this->match_file(http_request.uri)) > -1) {

            // send requested file
            this->send_file(remote_sock);

            return 0;

        } else {

            // file not found
            this->send_404(CID, remote_sock, http_request.uri);

            return 0;

        }


    } else if (http_request.method == "HEAD") {
        
        if (http_request.uri == "/") {
            
            // send index.html header
            this->send_index(CID, remote_sock, true);
        
        } else {
            
            // send requested file's header
            this->set_file_headers(CID, remote_sock);
            
        }

    } else {

        http_response.http_code = "501 Not implemented";
        http_response.date = this->server_date();

        this->clients[CID].response << "HTTP/1.0 " << http_response.http_code << CRLF;
        this->clients[CID].response << "Date: " << http_response.date << CRLF;
        this->clients[CID].response << "Content-Type: " << http_response.content_type << "; charset=UTF-8" << CRLF;
        this->clients[CID].response << "Server: " << http_response.server << CRLF;
        this->clients[CID].response << "Connection: close" << CRLF << CRLF;

    }


    // send response
    if (send(remote_sock, this->clients[CID].response.str().c_str(), this->clients[CID].response.str().size() - 1, 0) < 0 && DEBUG)
        std::cout << "send() failed: " << WSAGetLastError() << std::endl;


    // close socket
    g_Server->close(remote_sock);

    return 0;
}


/*
 * Index "file"
 * 
 * Send fake "index.html" because we don't actually have the "index.html".
 * If we have shared files they are included in the response. After the response
 * is sent the connection is closed.
 */
void HTTPD::send_index(int CID, SOCKET remote_sock, bool only_headers) {
    HTTP_Response http_response;

    this->clients[CID].responseBody
            << "<!DOCTYPE html>"
            << "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\"><head>"
            << "<title>KWIVA - WEB Share client</title>"
            << "<style type=\"text/css\">"
            << "html, body { font-family:Helvetica,Arial,sans-serif; color:#333333 } "
            << "h1 { color:#666666; font-weight:normal; margin:20px 0px 30px; } "
            << "thead { text-align:left; font-weight:bold; } "
            << "tr { padding:6px 0; } "
            << "thead tr { border-bottom:2px solid #DDDDDD; display:block; } "
            << "tbody tr { text-align:left; border-bottom:1px solid #DDDDDD; display:block; } "
            << "tbody td { height:32px; } "
            << "tbody > tr:hover { background:#edf3f6; } "
            << "a { font-size:15px; color:#2B547D; text-decoration:none; } "
            << "a:hover { text-decoration:underline; } "
            << "#wrapper { text-align:center; font-size:13px; margin:auto; width:960px; line-height:140%; margin-bottom:40px; } "
            << ".path { color:#999999; } "
            << "</style>"
            << "</head><body>"
            << "<div id=\"wrapper\">"
            << "<h1>Shared files</h1>";

    if (this->files.empty()) {

        this->clients[CID].responseBody << "<p>No files are shared.</p>";

    } else {

        this->clients[CID].responseBody
                << "<table cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">"
                << "<thead><tr><td width=\"400\">Name</td><td width=\"100\">Size</td><td width=\"400\">Path</td></tr></thead>"
                << "<tbody>";

        for (int j = 0; j < this->files.size(); j++) {
            this->clients[CID].responseBody
                    << "<tr>"
                    << "<td width=\"400\"><a href=\"" << this->files[j]->uri << "\">" << this->files[j]->file_name << "</a></td>"
                    << "<td width=\"100\">" << this->files[j]->file_size.readable << "</td>"
                    << "<td width=\"400\" class=\"path\">" << this->files[j]->screen_name << "</td>"
                    << "</tr>";
        }

        this->clients[CID].responseBody << "</tbody></table>";
    }

    this->clients[CID].responseBody << "</div></body></html>";


    // construct HTTP headers
    http_response.date = this->server_date();

    this->clients[CID].response << "HTTP/1.0 " << http_response.http_code << CRLF;
    this->clients[CID].response << "Date: " << http_response.date << CRLF;
    this->clients[CID].response << "Content-Type: " << http_response.content_type << "; charset=UTF-8" << CRLF;
    this->clients[CID].response << "Server: " << http_response.server << CRLF;
    this->clients[CID].response << "Content-length: " << this->clients[CID].responseBody.str().size() << CRLF;
    this->clients[CID].response << "Connection: close" << CRLF << CRLF;
    
    if (!only_headers)
        this->clients[CID].response << this->clients[CID].responseBody.str();


    // send html
    if (send(remote_sock, this->clients[CID].response.str().c_str(), this->clients[CID].response.str().size(), 0) < 0 && DEBUG)
        std::cout << "send() failed while sending index: " << WSAGetLastError() << std::endl;


    // close socket
    g_Server->close(remote_sock);
}


/*
 * 404 not found
 * 
 * 404 response to requested file that doesn't matches any of
 * the this->files[INDEX].uri URI's.
 */
void HTTPD::send_404(int CID, SOCKET remote_sock, std::string uri) {
    HTTP_Response http_response;

    this->clients[CID].responseBody
            << "<!DOCTYPE html>"
            << "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\"><head>"
            << "<title>KWIVA - WEB Share client</title>"
            << "<style type=\"text/css\">"
            << "html, body { font-family:Helvetica,Arial,sans-serif; color:#333333 } "
            << "h1 { color:#666666; font-weight:normal; margin:20px 0px 30px; } "
            << "#wrapper { text-align:center; font-size:13px; margin:auto; width:960px; line-height:140%; margin-bottom:40px; } "
            << "</style>"
            << "</head><body>"
            << "<div id=\"wrapper\">"
            << "<h1>Not found</h1>"
            << "<p>The requested URL " << uri << " was not found on this server.</p>"
            << "</div></body></html>";

    
    // construct HTTP headers
    http_response.http_code = "404 Not found";
    http_response.date = this->server_date();

    this->clients[CID].response << "HTTP/1.0 " << http_response.http_code << CRLF;
    this->clients[CID].response << "Date: " << http_response.date << CRLF;
    this->clients[CID].response << "Content-Type: " << http_response.content_type << "; charset=UTF-8" << CRLF;
    this->clients[CID].response << "Server: " << http_response.server << CRLF;
    this->clients[CID].response << "Content-length: " << this->clients[CID].responseBody.str().size() << CRLF;
    this->clients[CID].response << "Connection: close" << CRLF << CRLF;
    this->clients[CID].response << this->clients[CID].responseBody.str();


    // send html
    if (send(remote_sock, this->clients[CID].response.str().c_str(), this->clients[CID].response.str().size(), 0) < 0 && DEBUG)
        std::cout << "send() failed while sending 404: " << WSAGetLastError() << std::endl;


    // close socket
    g_Server->close(remote_sock);
}


/*
 * Find requested file
 * 
 * Match http request uri against this->files[INDEX].uri in order
 * to see if we have the file requested, if yes returns file index,
 * -1 otherwise.
 */
int HTTPD::match_file(std::string uri) {

    for (int j = 0; j < this->files.size(); j++)
        if (this->files[j]->uri.compare(uri) == 0)
            return j;

    // file not found
    return -1;

}


/*
 * Set file headers
 * 
 * Sets the http headers needed for http response
 */
int HTTPD::set_file_headers(int CID, SOCKET sock) {
    HTTP_Response http_response;
    HTTP_Request http_request;
    int FID = -1;
    
    this->parse_http(http_request, this->clients[CID].request.str());
    
    // check if we got the file
    if ((FID = this->match_file(http_request.uri)) < 0) {
        this->send_404(CID, sock, http_request.uri);
        
        return -1;
    }
    
    
    http_response.http_code = "200 OK";
    http_response.content_type = "application/octet-stream";
    //http_response.content_type = "application/force-download";
    http_response.file_name = this->files[FID]->file_name;
    http_response.date = this->server_date();

    this->clients[CID].response << "HTTP/1.0 " << http_response.http_code << CRLF;
    this->clients[CID].response << "Date: " << http_response.date << CRLF;
    this->clients[CID].response << "Server: " << http_response.server << CRLF;
    this->clients[CID].response << "Content-Type: " << http_response.content_type << "; charset=UTF-8" << CRLF;
    this->clients[CID].response << "Content-Disposition: attachment; filename=\"" << http_response.file_name << "\"" << CRLF;
    this->clients[CID].response << "Content-length: " << this->files[FID]->file_size.bytes << CRLF;
    this->clients[CID].response << "Content-Transfer-Encoding: binary" << CRLF;
    this->clients[CID].response << "Connection: close" << CRLF << CRLF;
    
    
    if (http_request.method == "GET") {
        // send http headers
        if (send(sock, this->clients[CID].response.str().c_str(), this->clients[CID].response.str().size(), 0) < 0 && DEBUG)
            std::cout << "send() failed from send_file_header(): " << WSAGetLastError() << std::endl;

        // clear response buffer
        this->clients[CID].response.str("");
        
        // remember that headers are sent
        this->clients[CID].headersSent = true;
    }
    
    return 0;
}


/*
 * Sends the requested file
 * 
 * This function takes care of file HTTP headers and starts a new thread
 * if this->clients[INDEX].fileTransferInProgress is set to false and request
 * type is GET. If request method is HEAD sends only response headers.
 */
int HTTPD::send_file(SOCKET remote_sock) {
    HTTP_Response http_response;
    int CID = 0;
    int FID = -1;

    // find to which socket to respond
    for (CID = 0; CID < MAX_CLIENTS; CID++) {
        if (this->clients[CID].sock == remote_sock)
            if (this->clients[CID].file.id > -1 && this->clients[CID].hasFullRequest == true) {
                FID = this->clients[CID].file.id;

                break;
            } else {
                return -1;
            }

        // socket not found
        if (CID == MAX_CLIENTS - 1) {
            if (DEBUG)
                std::cout << "Socket not found in send_file()" << std::endl;

            g_Server->close(remote_sock);

            return -1;
        }
    }
    
    
    // if needed, construct and send HTTP headers
    if (this->clients[CID].headersSent == false)
        this->set_file_headers(CID, remote_sock);


    // read and send file
    if (this->clients[CID].fileTransferInProgress == false) {
        Thread_Info *ti = new Thread_Info;
        ti->server = this;
        ti->sock = remote_sock;

        _beginthread(&HTTPD::start_thread, 0, (void *)ti);
        
        this->clients[CID].fileTransferInProgress = true;
    }
    
    return 0;
}


/*
 * File sending thread
 * 
 * Is created from this->send_file() method. This method buffers file and
 * then sends the buffer through the socket.
 * 
 * This is needed so that main thread (applications GUI) doesn't freeze when
 * large files are processed.
 */
void *HTTPD::read_and_send(Thread_Info *ti) {
    int CID = 0;
    int FID = -1;
    SOCKET remote_sock = ti->sock;
    
    // find corresponding socket CID/FID
    for (CID = 0; CID < MAX_CLIENTS; CID++)
        if (this->clients[CID].sock == remote_sock) {
            FID = this->clients[CID].file.id;
            
            break;
        }
    
    
    std::ifstream file(this->files[FID]->screen_name.c_str(), std::ios::in | std::ios::binary);
    if (file.is_open()) {
        char *buffer = new char[SEND_BUFF];
        int result = 0;
        
        if (DEBUG)
            std::cout << "File transfer started from thread #" << GetCurrentThreadId() << " (" << this->files[FID]->file_name << ")" << std::endl;

        while (!file.eof()) {
            
            /* Don't reread data from file, if previous call to
             * winsock returned WSAEWOULDBLOCK error
             */
            if (result != SOCKET_ERROR) {
                memset(buffer, 0, SEND_BUFF); // clear buffer

                file.seekg(this->clients[CID].file.bytesSent, std::ios::beg); // seek file
                file.read(buffer, SEND_BUFF); // read data
            }

            if ((result = send(remote_sock, buffer, file.gcount(), 0)) == SOCKET_ERROR) {
                
                if (WSAGetLastError() == WSAEWOULDBLOCK) {
                    /* We should stop sending until remote say's it's ok to send,
                     * this IS a recoverable error, but then we'd needed to find
                     * a way to notify i/o thread to start sending again, anyway this
                     * is better than nothing :)
                     */
                    if (DEBUG)
                        std::cout << "File sending paused by remote. Waiting..." << std::endl;

                    continue;
                } else if (result == 0) {
                    std::cout << "This is first IF result = 0" << std::endl;
                    
                    break;
                } else {
                    // something bad happened
                    if (DEBUG)
                        std::cout << "File i/o thread send() failed: " << WSAGetLastError() << std::endl;

                    break;
                }
                
            } else if (result == 0) {
                // client closed connection
                std::cout << "This is second IF result = 0" << std::endl;

                break;
            }

            // bytes sent, remember
            this->clients[CID].file.bytesSent += result;
        }

        // cleanup
        delete[] buffer;
        file.close();
    }
    
    if (DEBUG)
        std::cout << "File closed." << std::endl;
    
    // close socket
    g_Server->close(remote_sock);
    
}


/*
 * Accept http connection
 * 
 * Accepts the connection if already accepted connection count is
 * less thant MAX_CLIENTS and stores the socket in the first free
 * INDEX of this->clients array.
 */
void HTTPD::accept_conn(SOCKET remote_sock) {

    if (this->nClient < MAX_CLIENTS) {
        int freeSock = 0;
        int size = sizeof (struct sockaddr_in);

        // find a free socket index in the socket array
        for (int i = 0; i < MAX_CLIENTS; i++)
            if (this->clients[i].sock == 0) {
                freeSock = i;
                break;
            }

        // accept socket[freeSock]
        this->clients[freeSock].sock = accept(remote_sock, &this->clients[freeSock].addr, &size);

        if (this->clients[freeSock].sock == INVALID_SOCKET) {

            std::cout << "Error accepting client: " << WSAGetLastError() << std::endl;
            g_Server->close(this->clients[freeSock].sock);

        } else {
            if (DEBUG)
                std::cout << "Client #" << this->nClient << " connected using socket #" << freeSock << std::endl;

            this->nClient++;
        }

    }

}

/*
 * Close client cocket
 * 
 * Closes the socket and resets all values
 */
void HTTPD::close(SOCKET client) {

    for (int i = 0; i < MAX_CLIENTS; i++)
        if (this->clients[i].sock == client) {

            // close socket, reset values
            closesocket(this->clients[i].sock);
            
            this->clients[i].sock = {0};
            this->clients[i].addr = {0};
            this->clients[i].request.str("");
            this->clients[i].response.str("");
            this->clients[i].responseBody.str("");
            this->clients[i].hasFullRequest = false;
            this->clients[i].headersSent = false;
            this->clients[i].fileTransferInProgress = false;
            this->clients[i].file.bytesSent = 0;
            this->clients[i].file.id = -1;

            if (DEBUG)
                std::cout << "Client #" << i << " disconnected" << std::endl << std::endl;

            this->nClient--;

            // client disconnected, exit
            break;
        }

}


/*
 * Close server cocket
 * 
 * Gracefully closes all client sockets and the server socket,
 * also calls WSACleanup() to clean up winsock.
 */
void HTTPD::exit() {

    for (int i = 0; i < MAX_CLIENTS; i++) {
        shutdown(this->clients[i].sock, SD_BOTH);
        closesocket(this->clients[i].sock);
    }

    shutdown(this->server_socket, SD_BOTH);
    closesocket(this->server_socket);

    // cleanup winsock
    WSACleanup();
}


/*
 * Add shared directory
 * 
 * Recursively adds files from the directory specified
 */
void HTTPD::share_directory(char *dir_path) {
    WIN32_FIND_DATA FindFileData;
    std::string fpath = dir_path;
    fpath += "\\*.*";

    HANDLE hFind = FindFirstFile(fpath.c_str(), &FindFileData);

    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            // if directory
            if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
                
                if (strcmp(".", FindFileData.cFileName) && strcmp("..", FindFileData.cFileName)) {
                    fpath = dir_path;
                    fpath += "\\";
                    fpath += FindFileData.cFileName;

                    this->share_directory((char *) fpath.c_str());
                }
                
            } else { // if file

                fpath = dir_path;
                fpath += "\\";
                fpath += FindFileData.cFileName;

                this->share_file((char *) fpath.c_str());
            }
            
        } while (FindNextFile(hFind, &FindFileData) != 0);

        FindClose(hFind);
    }
}


/*
 * Share file
 * 
 * Generates random file URI, gets file size and adds
 * shared file to the server's this->files vector.
 */
void HTTPD::share_file(char *filename) {
    WIN32_FILE_ATTRIBUTE_DATA fileData;
    Share *file = new Share;
    size_t pos;
    char random_char[] = "_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    // generate random uri for file
    file->uri = "/";
    for (int i = 0; i < 10; i++)
        file->uri += random_char[rand() % strlen(random_char)];

    // a bit confusing but this is the full file path
    file->screen_name = filename;

    // and this is the real file name :)
    pos = file->screen_name.find_last_of("\\");
    file->file_name = file->screen_name.substr(pos + 1);

    
    // should be safe for files >4 GB as well
    if (GetFileAttributesEx(filename, GetFileExInfoStandard, &fileData)) {
        file->file_size.bytes = ((__int64) fileData.nFileSizeHigh << 32) + fileData.nFileSizeLow;
    } else {
        file->file_size.bytes = 0;
    }

    // human readable file size
    StrFormatByteSize64A(file->file_size.bytes, file->file_size.readable, sizeof (file->file_size.readable));

    
    // add shared file to server
    this->files.push_back(file);


    // add shared file to gui listbox
    char *tmp = new char[4];
    sprintf(tmp, "%02d", files.size());
    int lvItemCount = ListView_GetItemCount(g_hListView);
    LV_ITEM lvItem;

    memset(&lvItem, 0, sizeof (lvItem));
    lvItem.mask = LVIF_TEXT;
    lvItem.cchTextMax = 256;

    // #
    lvItem.iItem = lvItemCount;
    lvItem.pszText = tmp;
    SendMessage(g_hListView, LVM_INSERTITEM, 0, (LPARAM) & lvItem);

    // Path
    lvItem.iSubItem = 1;
    lvItem.pszText = filename;
    SendMessage(g_hListView, LVM_SETITEM, 0, (LPARAM) & lvItem);
    SendMessage(g_hListView, LVM_INSERTITEM, 0, (LPARAM) & lvItem);

    // Size
    lvItem.iSubItem = 2;
    lvItem.pszText = file->file_size.readable;
    SendMessage(g_hListView, LVM_SETITEM, 0, (LPARAM) & lvItem);

    delete [] tmp;
}


/*
 * Returns server date, e.g, the system time
 */
std::string HTTPD::server_date() {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80] = {0};

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    // Mon, 23 May 2005 22:38:34 GMT
    strftime(buffer, 80, "%a, %d %B %Y %H:%M:%S %Z", timeinfo);

    return buffer;
}

