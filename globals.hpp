/* 
 * File:   globals.hpp
 * Author: Roman Tomjak
 *
 * Created on May 15, 2012, 6:26 PM
 * 
 * This file is part of KWIVA.
 *
 * KWIVA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * KWIVA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with KWIVA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBALS_HPP
#define	GLOBALS_HPP

#define CRLF            "\r\n"          // http server <CR><LF>
#define DEBUG           0
#define VERSION         "1.0 ALPHA"     // server version
#define MAX_CLIENTS     10              // max connected clients
#define WM_SOCKET       WM_USER + 1     // so user message don't mix with windows messages
#define APP_WIDTH       600             // programs width
#define APP_HEIGHT      480             // and height in pixels

#include "server.hpp"

extern HTTPD *g_Server;
extern HWND g_hWnd, g_hInputBox, g_hListView, g_hStatusBar;

extern void die(char *);

#endif	/* GLOBALS_HPP */

